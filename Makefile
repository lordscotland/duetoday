ARCHS = arm64

include theos/makefiles/common.mk

TWEAK_NAME = DueToday
DueToday_FILES = Tweak.x
DueToday_FRAMEWORKS = EventKit

include $(THEOS_MAKE_PATH)/tweak.mk
