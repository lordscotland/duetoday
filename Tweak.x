#import <EventKit/EventKit.h>

%hook EKReminder
-(void)setCreationDate:(NSDate*)date {
  %orig;
  self.dueDateComponents=[[NSCalendar currentCalendar] components:NSEraCalendarUnit
   |NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
}
%end
